<?php
namespace Tracer;

Class Tracer {
    public static function trace ($var, $label = false) {

        if(is_object($var) || is_array($var)) {
            $var = print_r($var,1);
        }

        $label = $label ?: date('Y-m-d H:i:s');
        $url = env("TRACER_URL");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('trace' => $var, 'name' => $label)));
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        try {
            curl_exec($ch);
        } catch (\Exception $e) {

        }
    }
}
